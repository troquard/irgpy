#!/usr/bin/env python3

import sys
sys.path.append('../')

from irg import Player, Game
def main():


    hugo = Player("Hugo", [('A', 'B')], ['A'])
    ilene = Player("Ilene", [('B', 'C')], ['B'])
    juliet = Player("Juliet", [('A', 'C')], ['C'])
    game = Game(hugo, ilene, juliet)

    print(game)

    print("**** pca")
    for p in game.nash_generator_generic(game.prefers_pca):#, verbose=True):
        print("Nash:", p)
    print("**** ppca")
    for p in game.nash_generator_generic(game.prefers_ppca):#, verbose=True):
        print("Nash:", p)        
    print("**** pct")
    for p in game.nash_generator_generic(game.prefers_pct):#, verbose=True):
        print("Nash:", p)        
        
    
if __name__ == "__main__":
    main()
