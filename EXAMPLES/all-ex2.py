#!/usr/bin/env python3

import sys
sys.path.append('../')

from irg import Player, Game
def main():
    oscar = Player("Oscar", ['A', 'B'], ['B'])
    patrice = Player("Patrice", ['B','C'], ['A'])
    quentin = Player("Quentin", ['A', 'B'], ['C', 'B'])
    game = Game(oscar, patrice, quentin)
    print(game)
    print("**** pct")
    for p in game.nash_generator_generic(game.prefers_pct):#, verbose=True):
        print("Nash: " + str(p))
    print("**** ppca")
    for p in game.nash_generator_generic(game.prefers_ppca):#, verbose=True):
        print("Nash: " + str(p))
    print("**** pca")
    for p in game.nash_generator_generic(game.prefers_pca):#, verbose=True):
        print("Nash: " + str(p))
    
if __name__ == "__main__":
    main()
