#!/usr/bin/env python3

import sys
sys.path.append('../')

from irg import Player, Game


"""In presence of parsimonious controversy-averse preferences, we get:

Nash: Profile: [(0, '{A}'), (1, '{}'), (2, '{B, B}')] 
Nash: Profile: [(0, '{A}'), (1, '{B}'), (2, '{B}')] 
Nash: Profile: [(0, '{A}'), (1, '{B, B}'), (2, '{}')] 
Nash: Profile: [(0, '{A, B}'), (1, '{}'), (2, '{B}')] 
Nash: Profile: [(0, '{A, B}'), (1, '{B}'), (2, '{}')]

"""
def main():
    ann = Player("Ann", ['A','B'], ['A'])
    bob = Player("Bob", ['A', 'B', 'B'], ['B'])
    game = Game(ann, bob, bob)
    print(game)
    for p in game.nash_generator_generic(game.prefers_pca):
        print("Nash: " + str(p))
    
if __name__ == "__main__":
    main()
