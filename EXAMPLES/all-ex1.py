#!/usr/bin/env python3

import sys
sys.path.append('../')

from irg import Player, Game
def main():
    ann = Player("Ann", ['A','B'], ['A'])
    bob = Player("Bob", ['A', 'B', 'B'], ['B'])
    game = Game(ann, bob, bob)
    print(game)
    print("**** pct")
    for p in game.nash_generator_generic(game.prefers_pct):
        print("Nash: " + str(p))
    print("**** ppca")
    for p in game.nash_generator_generic(game.prefers_ppca):
        print("Nash: " + str(p))
    print("**** pca")
    for p in game.nash_generator_generic(game.prefers_pca):
        print("Nash: " + str(p))
    
if __name__ == "__main__":
    main()
