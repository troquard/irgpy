#!/usr/bin/env python3

import sys
sys.path.append('../')

from irg import Player, Game
import irg



def main():
    ann = Player("Ann", ['B', ('B','C'), 'B'], ['B','B'])
    bob = Player("Bob", ['B', 'K'], ['B', 'K'])
    game = Game(ann, bob)
    print(game)
    print("*** no complex resources")
    irg.COMPLEX = False
    for p in game.nash_generator_generic(game.prefers_pca):
        print("Nash: " + str(p))
    print("*** complex resources")        
    irg.COMPLEX = True
    for p in game.nash_generator_generic(game.prefers_pca):
        print("Nash: " + str(p))        
    
if __name__ == "__main__":
    main()
