#!/usr/bin/env python3

import sys
sys.path.append('../')

import itertools

from irg import Player, Game
def main():
    oscar = Player("Oscar", [('A', 'B'), 'B'], ['A'])
    patrice = Player("Patrice", [], ['B'])
    quentin = Player("Quentin", [], ['B'])
    game = Game(oscar, patrice, quentin)
    print(game)
    print("**** pct")
    for p in game.nash_generator_generic(game.prefers_pct):#, verbose=True):
        print("Nash:", p)
    print("**** ppca")
    for p in game.nash_generator_generic(game.prefers_ppca):#, verbose=True):
        print("Nash:", p)
    print("**** pca")
    for p in game.nash_generator_generic(game.prefers_pca):#, verbose=True):
        print("Nash:", p)

    print("**** Nash profiles with ppca but not with pca")    
    choices = [game.choices_generator(player) for player in game.players]
    for chs in itertools.product(*choices):
        profile = Game.Profile(game, chs)
        if (game.is_nash_generic(profile, game.prefers_ppca)
            and not game.is_nash_generic(profile, game.prefers_pca)):
            print(profile)
    print("**** Nash profiles with pct but not with ppca")
    choices = [game.choices_generator(player) for player in game.players]
    for chs in itertools.product(*choices):
        profile = Game.Profile(game, chs)
        if (game.is_nash_generic(profile, game.prefers_pct)
            and not game.is_nash_generic(profile, game.prefers_ppca)):
            print(profile)
        
    
if __name__ == "__main__":
    main()
