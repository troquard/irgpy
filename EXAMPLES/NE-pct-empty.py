#!/usr/bin/env python3

import sys
sys.path.append('../')

from irg import Player, Game
def main():
    # A complex example
    # oscar = Player("Oscar", ['A', 'B'], ['B', 'B'])
    # patrice = Player("Patrice", ['C'], ['A'])
    # quentin = Player("Quentin", ['A', 'B', 'C'], ['C', 'B'])
    # game = Game(oscar, patrice, quentin)
    # print(game)

    # A simple example
    theophile = Player("Theophile", ['A'], ['A'])
    ugo = Player("Ugo", ['A'], ['A', 'A'])
    game = Game(theophile, ugo)
    print(game)

    print("**** pca")
    for p in game.nash_generator_generic(game.prefers_pca):#, verbose=True):
        print("Nash:", p)
    print("**** ppca")
    for p in game.nash_generator_generic(game.prefers_ppca):#, verbose=True):
        print("Nash:", p)
    print("**** pct")
    for p in game.nash_generator_generic(game.prefers_pct, verbose=True):
        print("Nash:", p)

    
if __name__ == "__main__":
    main()
