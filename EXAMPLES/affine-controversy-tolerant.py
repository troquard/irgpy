#!/usr/bin/env python3

import sys
sys.path.append('../')

from irg import Player, Game


"""In presence of affine controversy-tolerant preferences, there are better-than-generic algorithms to determine whether a profile is a Nash equilibrium. See arXiv:1802.00257v1 Algorithm 2 and Algorithm 6.

In the case of affine dichotomous controversy-tolerant, we get 119 Nash equilibria.

In the case of affine parsimonious controversy-tolerant, we get:
Nash: Profile: [(0, '{A}'), (1, '{}'), (2, '{B}')]
Nash: Profile: [(0, '{A}'), (1, '{B}'), (2, '{}')]

Parsimony leads players 2 and 3 to prefer profiles that are controversial.

"""
def main():
    ann = Player("Ann", ['A','B'], ['A'])
    bob = Player("Bob", ['A', 'B', 'B'], ['B'])
    game = Game(ann, bob, bob)
    print(game)
    print("*** Affine dichotomic controversy-tolerant")
    for p in game.nash_generator_adct():
        print("Nash: " + str(p))
    print("*** Affine parsimonious controversy-tolerant")
    for p in game.nash_generator_apct():
        print("Nash: " + str(p))
        
if __name__ == "__main__":
    main()
