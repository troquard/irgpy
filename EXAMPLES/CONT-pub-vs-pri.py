#!/usr/bin/env python3

import sys
sys.path.append('../')
from multiset import Multiset

from irg import Player, Game
def main():
    oscar = Player("Oscar", [('A', 'B')], ['A'])
    patrice = Player("Patrice", [], ['B'])
    quentin = Player("Quentin", [], ['B'])
    game = Game(oscar, patrice, quentin)
    print(game)

    profile = Game.Profile(game, [Multiset([('A', 'B')]), Multiset(), Multiset()])
    print (profile, "is {yesno} (publicly) contentious."
           .format(yesno= "" if game.is_contentious(profile) else "not"))
    for id,player in [(0,oscar), (1, patrice), (2, quentin)]:
        print (profile, "is {yesno} privately contentious to"
               .format(yesno= "" if game.is_privately_contentious(profile, (id,player)) else "not"), player.name)

    
if __name__ == "__main__":
    main()
