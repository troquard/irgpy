#!/usr/bin/env python3

import sys
sys.path.append('../')

from irg import Player, Game
def main():
    oscar = Player("Oscar", [('A', 'A'), ('A', 'A')], ['A'])
    patrice = Player("Patrice", [], ['A', 'A'])
    quentin = Player("Quentin", ['A', 'B'], ['A', 'A', 'B'])
    game = Game(oscar, patrice, quentin)
    print(game)

    print("**** pca")
    for p in game.nash_generator_generic(game.prefers_pca, verbose=True):
        print("Nash:", p)
    print("**** ppca")
    for p in game.nash_generator_generic(game.prefers_ppca, verbose=True):
        print("Nash:", p)
    print("**** pct")
    for p in game.nash_generator_generic(game.prefers_pct, verbose=True):
        print("Nash:", p)
        
    
if __name__ == "__main__":
    main()
