Tools for finding Nash equilibrium solutions in Resource Contribution Games.

# Prerequisites

You will need Python3, and the `multiset` package. The latter can be installed with

```
pip3 install multiset
```

# Description

Tools for finding Nash equilibrium solutions in Resource Contribution Games (AMAI 2024). Resource Contribution Games correspond to Affine Individual Resource Games (IJCAI 2016) over MULT-, the fragment of Linear Logic where `A::p|A*A`.

It offers the following preference functions:

- dichotomous contention-tolerant (`prefers_dct`)
- parsimonious contention-tolerant (`prefers_pct`)
- dichotomous contention-averse (`prefers_dca`)
- parsimonious contention-averse (`prefers_pca`)
- dichotomous privately contention-averse (`prefers_dpca`)
- parsimonious privately contention-averse (`prefers_ppca`)

# Generic algorithm for Nash Equilibria verification and generation

A generic algorithm is implemented to check whether a profile is a Nash equilibrium.
We can define the IRG 
```
ann = Player("Ann", ['A','B'], ['A', 'B'])
bob = Player("Bob", ['A','B'], ['B'])
game = Game(ann, bob)
```

Working with parsimonious contention-averse preferences we can find all Nash equilibria
```
for p in game.nash_generator_generic(game.prefers_pca):
    print("Nash:", p)
>>> Nash: Profile: [(0, '{A}'), (1, '{B}')]
>>> Nash: Profile: [(0, '{A, B}'), (1, '{}')]
```

# Efficient algorithms for contention-tolerant preferences

An efficient algorithm is implemented to check whether a profile is a Nash equilibrium for:
- *Affine* IRG with dichotomous contention-tolerant (`is_nash_adct`)
- *Affine* IRG with parsimonious contention-tolerant (`is_nash_apct`)

Working with parsimonious contention-tolerant preferences we can find all Nash equilibria
```
for p in game.nash_generator_apct():
    print("Nash:", p)
>>> Nash: Profile: [(0, '{A, B}'), (1, '{B}')]
```

# Efficient algorithms for contention-averse preferences when endowments are bags of atomic resources

Efficient algorithms are implemented to check whether a profile is a Nash Equilibrium when:

- the endowments are bags of atomic and resources, and
- the preferences are contention-averse: `is_nash_pca_irgbar` and `is_nash_ppca_irgbar`.

Working with parsimonious (private or public) contention-averse
preferences and endowments as bags of atomics resources we can find
all Nash equilibria with `nash_generator_ppca_irgbar` and `nash_generator_ppca_irgbar`.

Working with parsimonious *public* contention-averse preferences and
endowments as bags of atomic resources, we can efficiently find a Nash
equilibrium with `find_nash_pca_irgbar`. With *private*
contention-averse preferences, one can *carefully* use
`find_nash_ppca_irgbar` which may raise a `LoopingComputationsException`.

# References

- **AMAI 2024** Nicolas Troquard. Existence and verification of Nash equilibria in non-cooperative contribution games with resource contention. In Annals of Mathematics and Artificial Intelligence , 92, pages 317–353. Springer, 2024. https://link.springer.com/article/10.1007/s10472-023-09905-7

- **IJCAI 2016** Nicolas Troquard. Nash equilibria and their elimination in resource games. In 25th International Joint Conference on Artificial Intelligence (IJCAI 2016). AAAI Press, 2016, pages 503-509. https://www.ijcai.org/Abstract/16/078

# TODO

TODO: bring support for full MULT, that is with the vacuous resource 1, `A::1|p|A*A`.

TODO: bring support for linear MULT. See `.issubset` in `is_individually_satisfied` and `is_controversial`.

TODO: add efficient algorithms for linear MULT and other preferences

