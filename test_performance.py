#!/usr/bin/env python3

import unittest

from multiset import FrozenMultiset, Multiset
from irg import Player, Game
import sys
import time
import random

utest = unittest.TestCase()

DEBUG = False
LOG = True
VERBOSE = True

def print_verbose(*objects, sep=' ', end='\n', file=sys.stdout, flush=False):
    if (VERBOSE):
        print(*objects, sep=sep, end=end, file=file, flush=flush)

def log(filename, logline):
    if (LOG):
        f = open("log-" + filename, "a+")
        f.write(logline + "\n")
        f.close()

def generate_player(NUMP, SIZEE, SIZEG):
    randplayer = Player(f"rand-{random.randint(0, 9)}{random.randint(0, 9)}{random.randint(0, 9)}",
                        'A' * random.randint(0, SIZEE) + 'B'  * random.randint(0, SIZEE) + 'C'  * random.randint(0, SIZEE) + 'D'  * random.randint(0, SIZEE) + 'E'  * random.randint(0, SIZEE) + 'F'  * random.randint(0, SIZEE),
                        'A' * random.randint(0, SIZEG) + 'B'  * random.randint(0, SIZEG) + 'C'  * random.randint(0, SIZEG) + 'D'  * random.randint(0, SIZEG) + 'E'  * random.randint(0, SIZEG) + 'F'  * random.randint(0, SIZEG))
    return randplayer

def generate_profile(game):
    choices = []
    for (id, p) in game.players:
        cp = Multiset()
        for e in p.endowment:
            if (random.choice([True, False])):
                cp.add(e)
        choices.append(cp)  # this will generate profiles with choices that tend to be about half the size of the size of the endowments.
    # choices = [random.choice(list(game.choices_generator(player))) for player in game.players]  # this is a very slow generation
    return Game.Profile(game, choices)


def tests_debug():
    print("**** Launching basic tests")
    import test_irg as t
    t.test_player_creation()
    t.test_profile_creation_positive()
    t.test_profile_creation_negative()
    t.test_nash_apct()
    t.test_nash_adct()
    t.test_private_public_contention()
    t.test_private_contention_property1()
    t.test_exists_deviation_irgbar_ppca()
    print("**** Basic tests passed!")
    


def test_efficient_irgbar(max_num_players,
                          max_repeated_resources_endow,
                          max_repeated_resources_goal,
                          num_games):
    print("**** Launching tests irgbar")
    NUMP = max_num_players
    SIZEE = max_repeated_resources_endow
    SIZEG = max_repeated_resources_goal

    count_generic_ppca = 0
    count_irgbar_ppca = 0
    count_generic_pca = 0
    count_irgbar_pca = 0
    numtests = 0

    for _ in range(0, num_games):
                   
        randnumplayers = random.randint(1, NUMP)
        players = [generate_player(NUMP, SIZEE, SIZEG)
                   for i in range(randnumplayers)]
        print_verbose("Selecting a random game and a random profile...")
        game = Game(*players)
        profile = generate_profile(game)
        print_verbose(game, profile)

        print_verbose("... solving isNE irgbar ppca")
        mcount = time.perf_counter()
        irgbar_ppca = game.is_nash_ppca_irgbar(profile)
        count_irgbar_ppca += (time.perf_counter() - mcount)
        print_verbose("... solving isNE generic ppca")        
        mcount = time.perf_counter()
        generic_ppca = game.is_nash_generic(profile, game.prefers_ppca)
        count_generic_ppca += (time.perf_counter() - mcount)
        utest.assertTrue(generic_ppca == irgbar_ppca)

        # print_verbose("... solving isNE irgbar pca")
        # mcount = time.perf_counter()
        # irgbar_pca = game.is_nash_pca_irgbar(profile)
        # count_irgbar_pca += (time.perf_counter() - mcount)
        # print_verbose("... solving isNE generic pca")        
        # mcount = time.perf_counter()
        # generic_pca = game.is_nash_generic(profile, game.prefers_pca)
        # count_generic_pca += (time.perf_counter() - mcount)
        # utest.assertTrue(generic_pca == irgbar_pca)
        
        numtests += 1
        if (numtests % 1 == 0):
            print_verbose("Number tests done (NE):", numtests)
            print_verbose("Time generic ppca:", count_generic_ppca)
            print_verbose("Time irgbar ppca:", count_irgbar_ppca)
            print_verbose("Time generic pca:", count_generic_pca)
            print_verbose("Time irgbar pca:", count_irgbar_pca)

    print("**** Tests irgbar done")
    print("Time generic ppca:", count_generic_ppca)
    print("Time irgbar ppca:", count_irgbar_ppca)
    print("Time generic pca:", count_generic_pca)
    print("Time irgbar pca:", count_irgbar_pca)
    return (count_generic_ppca, count_irgbar_ppca, count_generic_pca, count_irgbar_pca)
    
def test_efficient_ct():
    print("**** Launching tests contention tolerant")
    filename = ""
    if (LOG):
        filename = "result." + str(datetime.datetime.now())

    ann = Player("Ann", ['A', 'B', 'E', 'B'], ['A', 'B'])
    bob = Player("Bob", ['A', 'B', 'C'], ['B'])
    charly = Player("Charly", ['B', 'E'], ['E'])
    daniel = Player("Daniel", ['A', ('A', 'E')], ['A', 'A', 'E'])
    edouard = Player("Edouard", ['B', ('A', 'B')], ['A', 'B', 'C'])
    game = Game(ann, bob, bob, charly, daniel, edouard, edouard)
    print_verbose(game)

    t = time.perf_counter()

    print("Efficient algorithm for affine IRG",
          "with contention-tolerant parsimonious preferences")
    nashs_airgpctp = set()
    for p in game.nash_generator_apct():
        print_verbose("Nash: " + str(p))
        nashs_airgpctp.add(p)
    print("Time: " + str(time.perf_counter() - t))
    print_verbose("Found: " + str(len(nashs_airgpctp)))

    t = time.perf_counter()
    print("Generic algorithm")
    nashs_generic = set()
    for p in game.nash_generator_generic(game.prefers_pct):
        print_verbose("Nash: " + str(p))
        nashs_generic.add(p)
    print("Time: " + str(time.perf_counter() - t))
    print_verbose("Found: " + str(len(nashs_generic)))

    utest.assertTrue(nashs_airgpctp.issubset(nashs_generic))
    utest.assertTrue(nashs_generic.issubset(nashs_airgpctp))
    print("**** Tests ct done")


if __name__ == "__main__":
    if (DEBUG):
        tests_debug()
    # test_efficient_ct()

    # parameters of random tests: max_num_players,
    # max_repeated_resources_endow, max_repeated_resources_goal,
    # num_games
    log("pca-vs-generic", "parameters of random tests: max_num_players, max_repeated_resources_endow, max_repeated_resources_goal, num_games")
    for repetitions in range(1, 20):
        log("ppca-vs-generic", f"*** 5, {repetitions}, 5, 100")
        (count_generic_ppca, count_irgbar_ppca, count_generic_pca, count_irgbar_pca) = test_efficient_irgbar(5, repetitions, 5, 100)
        log("ppca-vs-generic", f"generic {count_generic_ppca}")
        log("ppca-vs-generic", f"irgbar {count_irgbar_ppca}")        
        
