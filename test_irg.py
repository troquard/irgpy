#!/usr/bin/env python3

import unittest

from multiset import FrozenMultiset, Multiset
from irg import Player, Game, LoopingComputationsException
import sys
import time
import random

utest = unittest.TestCase()

VERBOSE = False

def print_verbose(*objects, sep=' ', end='\n', file=sys.stdout, flush=False):
    if (VERBOSE):
        print(*objects, sep=sep, end=end, file=file, flush=flush)

def test_player_creation():
    try:
        Player("Ann", ['gold', 'ice', 'bucket', 'ice'], ['diamond'])
        Player("Ann", [6, 4, 4, 3, 4], [2])
        Player("Ann", ['A', 'H', 'F', 'N', 'B', 'B'], ['Z', 'B'])
        Player("Ann", 'AHFNBB', 'ZB')
        Player("Ann", [(2, 1, 4), (2, 1, 4), (88, 88)], 'dlfkj')
    except Exception:
        assert False
    assert True


def test_profile_creation_positive():
    ann = Player("Ann", 'AHFNBB', 'ZB')
    bob = Player("Bob", 'ZHF', 'A')
    game = Game(ann, bob)
    try:
        cann = FrozenMultiset('NBABH')  # Ann's choice
        cbob = FrozenMultiset('H')  # Bob's choice
        profile = game.create_profile((cann, cbob))
        cannn = FrozenMultiset('NBAHF')
        cbobb = FrozenMultiset('HZF')
        profile = game.create_profile((cannn, cbobb))
    except Exception:
        assert False
    assert True


def test_profile_creation_negative():
    ann = Player("Ann", 'AHFNBB', 'ZB')
    bob = Player("Bob", 'ZHF', 'A')
    game = Game(ann, bob)
    with utest.assertRaises(Exception):
        cann = FrozenMultiset('NBABH')
        cbob = FrozenMultiset('HA')
        profile = game.create_profile((cann, cbob))
    with utest.assertRaises(Exception):
        cann = FrozenMultiset('NBABH')
        cbob = FrozenMultiset('HZM')
        profile = game.create_profile((cann, cbob))
    with utest.assertRaises(Exception):
        cann = Multiset('NBABHH')
        cbob = Multiset('HZF')
        profile = game.create_profile((cann, cbob))
    with utest.assertRaises(Exception):
        cann = FrozenMultiset('B')
        cbob = FrozenMultiset('HZ')
        profile = game.create_profile((cann, cann, cbob))


def test_outcome():
    '''Simple test of outcome with bundles.'''
    ann = Player("Ann", ['A', ('H', 'F'), ('H', 'F')], 'H')
    bob = Player("Bob", ['Z', 'H', 'H'], 'A')
    game = Game(ann, bob)
    cann = FrozenMultiset(['A', ('H', 'F')])
    cbob = FrozenMultiset('H')
    profile = game.create_profile((cann, cbob))
    utest.assertTrue(len(game.outcome(profile, complex=False)) == 3)
    utest.assertTrue(len(game.outcome(profile, complex=True)) == 4)

    ann = Player("Ann", ['AA', ('HH', 'FA'), ('HH', 'FA')], ['HH'])
    bob = Player("Bob", ['ZA', 'HE', 'HE'], ['A'])
    game = Game(ann, bob)
    cann = FrozenMultiset(['AA', ('HH', 'FA'), ('HH', 'FA')])
    cbob = FrozenMultiset(['HE'])
    profile = game.create_profile((cann, cbob))
    utest.assertEqual(len(game.outcome(profile, complex=False)), 4, profile)
    utest.assertEqual(len(game.outcome(profile, complex=True)), 6, profile)


def test_nash_adct():
    ann = Player("Ann", ['A', 'B', 'E', 'B'], ['A', 'B'])
    bob = Player("Bob", ['A', 'B'], ['B'])
    charly = Player("Charly", ['B', 'E'], ['E'])
    tests = [Game(ann), Game(ann, bob), Game(ann, bob, charly),
             Game(ann, bob, charly, ann)]

    for game in tests:
        nashs_airgdctp = set()
        for p in game.nash_generator_adct():
            nashs_airgdctp.add(p)
        nashs_generic = set()
        for p in game.nash_generator_generic(game.prefers_dct):
            nashs_generic.add(p)
        utest.assertTrue(nashs_airgdctp.issubset(nashs_generic))
        utest.assertTrue(nashs_generic.issubset(nashs_airgdctp))


def test_nash_apct():
    ann = Player("Ann", ['A', 'B', 'E', 'B'], ['A', 'B'])
    bob = Player("Bob", ['A', 'B'], ['B'])
    charly = Player("Charly", ['B', 'E'], ['E'])
    tests = [Game(ann), Game(ann, bob), Game(ann, bob, charly),
             Game(ann, bob, charly, ann)]

    for game in tests:
        nashs_airgpctp = set()
        for p in game.nash_generator_apct():
            nashs_airgpctp.add(p)
        nashs_generic = set()
        for p in game.nash_generator_generic(game.prefers_pct):
            nashs_generic.add(p)
        utest.assertTrue(nashs_airgpctp.issubset(nashs_generic))
        utest.assertTrue(nashs_generic.issubset(nashs_airgpctp))


def test_private_public_contention():
    '''
    Test that a profile is contentious iff there is player for whom
    the profile is contentious.

    '''
    ann = Player("Ann", ['A', 'B', 'E', 'B'], ['A', 'B'])
    bob = Player("Bob", ['A', 'B'], ['B'])
    charly = Player("Charly", ['B', 'E'], ['E'])
    dora = Player("Dora", ['A', 'B', 'E', 'F'], ['A', 'A', 'E'])
    A = Player("Company A", ['3G', '3G', ('3G', '3G')], ['3G', '3G', '4G'])
    B = Player("Company B", ['3G', '4G', ('4G', '4G')], ['3G', '3G', '4G', '4G'])
    dinosaur = 'AAAB'
    aircraft = 'CD'
    tank = 'AEF'
    ship = 'AC'
    kid1 = Player("Kid 1", ['A', ('A', 'E', 'B')], dinosaur)
    kid2 = Player("Kid 2", [('A', 'C'), 'F', ('A', 'D')], aircraft)
    kid3 = Player("Kid 3", [('A', 'E', 'C')], tank)
    kid4 = Player("Kid 4", ['A', 'C', ('D', 'D')], ship)

    tests = [Game(ann), Game(ann, bob), Game(ann, bob, charly),
             Game(ann, bob, charly, ann), Game(ann, bob, dora),
             Game(dora), Game(dora, charly), Game(dora, dora, dora),
             Game(A, B), Game(kid1, kid2, kid3, kid4)]

    for game in tests:
        for p in game.profile_generator():
            for ply in game.players:
                if game.is_privately_contentious(p, ply):
                    utest.assertTrue(game.is_contentious(p),
                                     ("{}: program says that {} "
                                      "is privately contentious "
                                      "for {} but not contentious.")
                                     .format(game, p, ply))
            if game.is_contentious(p):
                also_privately_contentious = False
                for ply in game.players:
                    if (game.is_privately_contentious(p, ply)):
                        also_privately_contentious = True
                utest.assertTrue(also_privately_contentious,
                                 ("{}: program says that {} is contentious "
                                  "but not privately contentious.")
                                 .format(game, p))


def test_private_contention_property1():
    '''
    Test that when a profile is contentious for a player it is
    also contentious for another different player.
    '''
    ann = Player("Ann", ['A', 'B', 'E', 'B'], ['A', 'B'])
    bob = Player("Bob", ['A', 'B'], ['B'])
    charly = Player("Charly", ['B', 'E'], ['E'])
    dora = Player("Dora", ['A', 'B', 'E', 'F'], ['A', 'A', 'E'])
    elodie = Player("Elodie", [('A', 'A')], ['A'])
    fanon = Player("Fanon", ['A'], ['A'])
    A = Player("Company A", ['3G', '3G', ('3G', '3G')], ['3G', '3G', '4G'])
    B = Player("Company B", ['3G', '4G', ('4G', '4G')], ['3G', '3G', '4G', '4G'])
    dinosaur = 'AAAB'
    aircraft = 'CD'
    tank = 'AEF'
    ship = 'AC'
    kid1 = Player("Kid 1", ['A', ('A', 'E', 'B')], dinosaur)
    kid2 = Player("Kid 2", [('A', 'C'), 'F', ('A', 'D')], aircraft)
    kid3 = Player("Kid 3", [('A', 'E', 'C')], tank)
    kid4 = Player("Kid 4", ['A', 'C', ('D', 'D')], ship)

    tests = [Game(ann), Game(ann, bob), Game(ann, bob, charly),
             Game(ann, bob, charly, ann), Game(ann, bob, dora),
             Game(dora), Game(dora, charly),
             Game(dora, dora, dora), Game(elodie),
             Game(elodie, elodie), Game(elodie, elodie, elodie),
             Game(elodie, elodie, fanon),
             Game(A, B), Game(kid1, kid2, kid3, kid4)]

    for game in tests:
        for p in game.profile_generator():
            for ply in game.players:
                if game.is_privately_contentious(p, ply):
                    also_contentious_for_another_player = False
                    for _ply in game.players:
                        if (_ply != ply and
                            game.is_privately_contentious(p, _ply)):
                            also_contentious_for_another_player = True
                            break
                    utest.assertTrue(also_contentious_for_another_player)


def test_exists_deviation_irgbar_ppca():
    ann = Player("Ann", ['A', 'B', 'E', 'B'], ['A', 'B'])
    bob = Player("Bob", ['A', 'B'], ['B'])
    charly = Player("Charly", ['B', 'E'], ['E'])
    dora = Player("Dora", ['A', 'B', 'E', 'F'], ['A', 'A', 'E'])
    elodie = Player("Elodie", ['A', 'A'], ['A'])
    fanon = Player("Fanon", ['A'], ['A'])
    A = Player("Company A", ['3G', '3G', '3G', '3G'], ['3G', '3G', '4G'])
    B = Player("Company B", ['3G', '4G', '4G', '4G'], ['3G', '3G', '4G', '4G'])
    dinosaur = 'AAAB'
    aircraft = 'CD'
    tank = 'AEF'
    ship = 'AC'
    kid1 = Player("Kid 1", ['A', 'A', 'E', 'B'], dinosaur)
    kid2 = Player("Kid 2", ['A', 'C', 'F', 'A', 'D'], aircraft)
    kid3 = Player("Kid 3", ['A', 'E', 'C'], tank)
    kid4 = Player("Kid 4", ['A', 'C', 'D', 'D'], ship)
    rand649 = Player("rand-649", 'ABEF', 'AF')
    rand337 = Player("rand-337", 'BCF', 'F')
    
    tests = [Game(ann), Game(ann, bob), Game(ann, bob, charly),
             Game(ann, bob, charly, ann), Game(ann, bob, dora),
             Game(dora), Game(dora, charly),
             Game(rand649, rand337),
             Game(dora, dora, dora), Game(elodie),
             Game(elodie, elodie), Game(elodie, elodie, elodie),
             Game(elodie, elodie, fanon),
             Game(A, B), Game(kid1, kid2, kid3, kid4)
    ]
    for game in tests:
        for p in game.profile_generator():
            for ply in game.players:
                resirgbar = game.exists_profitable_deviation_ppca_irgbar(ply, p)
                resgeneric = game.exists_profitable_deviation_generic(ply, p, game.prefers_ppca)
                utest.assertTrue(resirgbar[0] == resgeneric[0])


def test_exists_deviation_irgbar_ppca1():
    p1 = Player("p1", 'ABBCDDEEFF', 'AE')
    p2 = Player("p2", 'AEEF', 'ACDF')
    p3 = Player("p3", 'AABDDEEF', 'BCDEF')
    game = Game(p1, p2, p3)
    print_verbose("... This will take some time...")    
    for p in game.profile_generator():    
        for ply in game.players:
            resirgbar = game.exists_profitable_deviation_ppca_irgbar(ply, p)
            resgeneric = game.exists_profitable_deviation_generic(ply, p, game.prefers_ppca)    
            utest.assertTrue(resirgbar[0] == resgeneric[0])
    
    
def generate_player(NUMP, SIZEE, SIZEG):
    randplayer = Player(f"rand-{random.randint(0, 9)}{random.randint(0, 9)}{random.randint(0, 9)}",
                        'A' * random.randint(0, SIZEE) + 'B'  * random.randint(0, SIZEE) + 'C'  * random.randint(0, SIZEE) + 'D'  * random.randint(0, SIZEE) + 'E'  * random.randint(0, SIZEE) + 'F'  * random.randint(0, SIZEE),
                        'A' * random.randint(0, SIZEG) + 'B'  * random.randint(0, SIZEG) + 'C'  * random.randint(0, SIZEG) + 'D'  * random.randint(0, SIZEG) + 'E'  * random.randint(0, SIZEG) + 'F'  * random.randint(0, SIZEG))
    return randplayer

def generate_profile(game):
    choices = [random.choice(list(game.choices_generator(player))) for player in game.players]
    return Game.Profile(game, choices)

def test_random_exists_deviation_irgbar_ppca(max_num_players,
                                             max_repeated_resources_endow,
                                             max_repeated_resources_goal,
                                             num_games):
    NUMP = max_num_players
    SIZEE = max_repeated_resources_endow
    SIZEG = max_repeated_resources_goal
    
    count_generic = 0
    count_irgbar = 0
    numtests = 0
    for _ in range(0, num_games):
        randnumplayers = random.randint(1, NUMP)
        players = [generate_player(NUMP, SIZEE, SIZEG)
                   for i in range(randnumplayers)]
        game = Game(*players)

        print_verbose(game)
        for p in game.profile_generator():
            for ply in game.players:
                numtests += 1
                mcount = time.perf_counter()
                resirgbar = game.exists_profitable_deviation_ppca_irgbar(ply, p)
                count_irgbar += (time.perf_counter() - mcount)
                mcount = time.perf_counter()
                resgeneric = game.exists_profitable_deviation_generic(ply, p, game.prefers_ppca)
                count_generic += (time.perf_counter() - mcount)
                if (numtests % 100 == 0):
                    print_verbose("Number tests done (EPD):", numtests)
                    print_verbose("Time generic:", count_generic)
                    print_verbose("Time irgbar:", count_irgbar)
                utest.assertTrue(resirgbar[0] == resgeneric[0])


def test_random_exists_nash_irgbar_ppca(max_num_players,
                                        max_repeated_resources_endow,
                                        max_repeated_resources_goal,
                                        num_games):
    NUMP = max_num_players
    SIZEE = max_repeated_resources_endow
    SIZEG = max_repeated_resources_goal

    count_generic = 0
    count_irgbar = 0    
    numtests = 0
    
    for _ in range(0, num_games):
        numtests += 1
        randnumplayers = random.randint(1, NUMP)
        players = [generate_player(NUMP, SIZEE, SIZEG)
                   for i in range(randnumplayers)]
        game = Game(*players)
        print_verbose(game)
        mcount = time.perf_counter()
        utest.assertTrue(any(game.is_nash_ppca_irgbar(p) for p in game.profile_generator()))
        print_verbose("PPCA IRGBAR OK")
        count_irgbar += (time.perf_counter() - mcount)
        mcount = time.perf_counter()        
        utest.assertTrue(any(game.is_nash_generic(p, game.prefers_ppca) for p in game.profile_generator()))
        count_generic += (time.perf_counter() - mcount)
        if (numtests % 1 == 0):
            print_verbose("Number tests done (ENE):", numtests)            
            print_verbose("Time generic:", count_generic)
            print_verbose("Time irgbar:", count_irgbar)


def test_random_exists_deviation_irgbar_pca(max_num_players,
                                             max_repeated_resources_endow,
                                             max_repeated_resources_goal,
                                             num_games):
    NUMP = max_num_players
    SIZEE = max_repeated_resources_endow
    SIZEG = max_repeated_resources_goal
    
    count_generic = 0
    count_irgbar = 0    
    numtests = 0
    for _ in range(0, num_games):
        randnumplayers = random.randint(1, NUMP)
        players = [generate_player(NUMP, SIZEE, SIZEG)
                   for i in range(randnumplayers)]
        game = Game(*players)
        print_verbose(game)
        for p in game.profile_generator():
            for ply in game.players:
                numtests += 1
                mcount = time.perf_counter()
                resirgbar = game.exists_profitable_deviation_pca_irgbar(ply, p)
                count_irgbar += (time.perf_counter() - mcount)
                mcount = time.perf_counter()
                resgeneric = game.exists_profitable_deviation_generic(ply, p, game.prefers_pca)
                count_generic += (time.perf_counter() - mcount)
                if (numtests % 1000 == 0):
                    print_verbose("Number tests done (EPD):", numtests)
                    print_verbose("Time generic:", count_generic)
                    print_verbose("Time irgbar:", count_irgbar)
                utest.assertTrue(resirgbar[0] == resgeneric[0], f'Unexpected result EPD^pca({game}, {p}, {ply}). Should be {resgeneric[0]}.')


def test_random_exists_nash_irgbar_pca(max_num_players,
                                       max_repeated_resources_endow,
                                       max_repeated_resources_goal,
                                       num_games):
    NUMP = max_num_players
    SIZEE = max_repeated_resources_endow
    SIZEG = max_repeated_resources_goal

    count_generic = 0
    count_irgbar = 0    
    numtests = 0
    
    for _ in range(0, num_games):
        numtests += 1
        randnumplayers = random.randint(1, NUMP)
        players = [generate_player(NUMP, SIZEE, SIZEG)
                   for i in range(randnumplayers)]
        game = Game(*players)
        print_verbose(game)
        mcount = time.perf_counter()
        utest.assertTrue(any(game.is_nash_pca_irgbar(p) for p in game.profile_generator()))
        print_verbose("PCA IRGBAR OK")
        count_irgbar += (time.perf_counter() - mcount)
        mcount = time.perf_counter()
        utest.assertTrue(any(game.is_nash_generic(p, game.prefers_pca) for p in game.profile_generator()))
        count_generic += (time.perf_counter() - mcount)
        if (numtests % 1 == 0):
            print_verbose("Number tests done (ENE):", numtests)            
            print_verbose("Time generic:", count_generic)
            print_verbose("Time irgbar:", count_irgbar)
            


def test_find_nash_irgbar_pca():
    ann = Player("Ann", ['A', 'B', 'E', 'B'], ['A', 'B'])
    bob = Player("Bob", ['A', 'B'], ['B'])
    charly = Player("Charly", ['B', 'E'], ['E'])
    dora = Player("Dora", ['A', 'B', 'E', 'F'], ['A', 'A', 'E'])
    elodie = Player("Elodie", ['A', 'A'], ['A'])
    fanon = Player("Fanon", ['A'], ['A'])
    A = Player("Company A", ['3G', '3G', '3G', '3G'], ['3G', '3G', '4G'])
    B = Player("Company B", ['3G', '4G', '4G', '4G'], ['3G', '3G', '4G', '4G'])
    dinosaur = 'AAAB'
    aircraft = 'CD'
    tank = 'AEF'
    ship = 'AC'
    kid1 = Player("Kid 1", ['A', 'A', 'E', 'B'], dinosaur)
    kid2 = Player("Kid 2", ['A', 'C', 'F', 'A', 'D'], aircraft)
    kid3 = Player("Kid 3", ['A', 'E', 'C'], tank)
    kid4 = Player("Kid 4", ['A', 'C', 'D', 'D'], ship)
    
    tests = [Game(ann), Game(ann, bob), Game(ann, bob, charly),
             Game(ann, bob, charly, ann), Game(ann, bob, dora),
             Game(dora), Game(dora, charly),
             Game(dora, dora, dora), Game(elodie),
             Game(elodie, elodie), Game(elodie, elodie, elodie),
             Game(elodie, elodie, fanon)#,
             #Game(A, B), Game(kid1, kid2, kid3, kid4)
    ]
    for game in tests:
        candidate = game.find_nash_pca_irgbar()
        utest.assertTrue(game.is_nash_generic(candidate, game.prefers_pca))

def test_random_find_nash_irgbar_pca(max_num_players,
                                      max_repeated_resources_endow,
                                      max_repeated_resources_goal,
                                      num_games):
    NUMP = max_num_players
    SIZEE = max_repeated_resources_endow
    SIZEG = max_repeated_resources_goal
    
    for _ in range(0, num_games):
        randnumplayers = random.randint(1, NUMP)
        players = [generate_player(NUMP, SIZEE, SIZEG)
                   for i in range(randnumplayers)]
        game = Game(*players)

        candidate = game.find_nash_pca_irgbar()
        print_verbose("CONSIDERING", game)
        print_verbose("FOUND NASH CANDIDATE", candidate)
        utest.assertTrue(game.is_nash_generic(candidate, game.prefers_pca))
        print_verbose("OK")


def test_random_find_nash_irgbar_pca_try_falsify(max_num_players,
                                                 max_repeated_resources_endow,
                                                 max_repeated_resources_goal,
                                                 num_games):
    '''Try to falsify the fact that there are always NE in IRGBAR with PCA.'''
    NUMP = max_num_players
    SIZEE = max_repeated_resources_endow
    SIZEG = max_repeated_resources_goal
    
    for _ in range(0, num_games):
        randnumplayers = random.randint(1, NUMP)
        players = [generate_player(NUMP, SIZEE, SIZEG)
                   for i in range(randnumplayers)]
        game = Game(*players)
        print_verbose(f"PCA{_} CONSIDERING", game)
        try:
            candidate = game.find_nash_pca_irgbar()
            # utest.assertTrue(game.is_nash_generic(candidate, game.prefers_pca))
            print_verbose(f"Found {candidate}")
        except LoopingComputationsException:
            #  ...should never even get here, if our find_nash_pca_irgbar is correct
            utest.assertTrue(False)
            # print_verbose("Did not find one with incomplete algorithm...")
            # utest.assertTrue(any([True for _ in game.nash_generator_pca_irgbar()]))
        print_verbose("OK")

        
def test_no_nash_irgbar_ppca():
    '''Show that with private contention-averse preferences, an IRGBAR might not have any Nash equilibria.
    This game should not have any Nash equilibria: Game: ['(1, Player: Name: rand-991. Endowment: {A, B}. Goal: {A, A, B, B}.)', '(2, Player: Name: rand-204. Endowment: {A, A, A}. Goal: {A, B}.)', '(3, Player: Name: rand-724. Endowment: {B, B}. Goal: {B}.)', '(4, Player: Name: rand-466. Endowment: {A, A, A, B}. Goal: {A}.)']
    We consider a simplified game.
    '''
    one = Player("one", '', 'AABB') # endowment was AB
    two = Player("two", '', 'AB') # endowment was AAA
    three = Player("three", 'BB', 'B')
    four = Player("four", 'AA', 'A') # endowment was AABB
    game = Game(one, two, three, four)
    #utest.assertFalse(any([True for _ in game.nash_generator_ppca_irgbar()]))
    utest.assertFalse(any([True for _ in game.nash_generator_generic(game.prefers_ppca, verbose=False)]))
    
def main():
    RANDOM_TESTS = True
    # parameters of random tests: max_num_players,
    # max_repeated_resources_endow, max_repeated_resources_goal,
    # num_games

    print("Basic tests.")
    test_player_creation()
    test_profile_creation_positive()
    test_profile_creation_negative()
    test_outcome()
    test_nash_adct()
    test_nash_apct()
    test_private_public_contention()
    test_private_contention_property1()
    test_exists_deviation_irgbar_ppca()
    # test_exists_deviation_irgbar_ppca1()  # takes a long time
    test_find_nash_irgbar_pca()    
    test_no_nash_irgbar_ppca()
    print("Basic tests done.")
    if (RANDOM_TESTS):
        print("Tests random exists deviation irgbar pca/ppca.")
        # parameter for endowment must be small, or it takes a long time
        #test_random_exists_deviation_irgbar_ppca(2, 1, 3, 10)
        test_random_exists_deviation_irgbar_ppca(3, 2, 1, 5)
        #test_random_exists_deviation_irgbar_ppca(3, 1, 1, 10)
        #test_random_exists_deviation_irgbar_pca(2, 1, 3, 10)
        test_random_exists_deviation_irgbar_pca(3, 2, 1, 5)
        #test_random_exists_deviation_irgbar_pca(3, 1, 1, 10)
        # test_random_exists_deviation_irgbar_pca(3, 6, 2, 5)  # takes a long time
        print("Tests random exists deviation irgbar pca/ppca done.")
        pass
    if (RANDOM_TESTS):
        print("Tests random exists Nash irgbar pca/ppca.")
        #test_random_exists_nash_irgbar_ppca(2, 3, 2, 10)
        test_random_exists_nash_irgbar_ppca(3, 2, 2, 10)
        test_random_exists_nash_irgbar_pca(2, 3, 2, 10)
        #test_random_exists_nash_irgbar_pca(3, 2, 2, 10)
        print("Tests random exists Nash irgbar pca/ppca done.")
        pass
    if (RANDOM_TESTS):
        print("Tests random exists Nash irgbar pca.")
        test_random_find_nash_irgbar_pca(5, 4, 1, 50)
        print("Tests random exists Nash irgbar pca.")
        pass
    if (RANDOM_TESTS):
        print("Tests random try falsify find Nash irgbar pca.")
        # test_random_find_nash_irgbar_pca_try_falsify(100, 8, 4, 1000)
        test_random_find_nash_irgbar_pca_try_falsify(12, 8, 4, 1000)
        pass

    print("Success!")


if __name__ == "__main__":
    main()
