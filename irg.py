'''Tools for finding Nash equilibrium solutions in Affine Individual
Resource Games over MULT, where: MULT is the fragment of Linear Logic
where A::p|1|A*A.

TODO: bring support for full MULT, that is with the vacuous resource 1
TODO: bring support for linear MULT. See .issubset in
is_individually_satisfied and is_contentious.


It offers the following preference functions:
- dichotomous contention-tolerant (prefers_dct)
- parsimonious contention-tolerant (prefers_pct)
- dichotomous (public) contention-averse (prefers_dca)
- parsimonious (public) contention-averse (prefers_pca)
- dichotomous private contention-averse (prefers_dpca)
- parsimonious private contention-averse (prefers_ppca)


* GENERIC ALGORITHMS

A generic algorithm is implemented to check whether a profile
is a Nash equilibrium. We can define the IRG
ann = Player("Ann", ['A','B'], ['A', 'B'])
bob = Player("Bob", ['A','B'], ['B'])
game = Game(ann, bob)
Working parsimonious public contention-averse preferences
we can find all Nash equilibria
for p in game.nash_generator_generic(game.prefers_pca):
    print("Nash:", p)
>>> Nash: Profile: [(0, '{A}'), (1, '{B}')]
>>> Nash: Profile: [(0, '{A, B}'), (1, '{}')]


* EFFICIENT ALGORITHMS

An efficient algorithm is implemented to check whether a profile
is a Nash equilibrium for:
- *Affine* IRG with parsimonious contention-tolerant. C.f., is_nash_apct.

Working parsimonious contention-tolerant preferences
we can find all Nash equilibria
for p in game.nash_generator_apct():
    print("Nash:", p)
>>> Nash: Profile: [(0, '{A, B}'), (1, '{B}')]

- *Affine* IRGBARs (IRG with endowments as bags of atomic resources)
   with parsimonious (public and private) contention-averse
   preferences. C.f., is_nash_pca_irgbar, and is_nash_ppca_irgbar.

An efficient algorithm is implemented to *find* an (always existing)
Nash equilibrium in IRGBARs in presence of public contention-averse
preferences. C.f., find_nash_pca_irgbar.


* ASSUMPTIONS
Resources can be disposed of freely. If a player is individually satisfied
with {'R'}, then they are individually satisfied with {'R', 'A'}.
Resources ('A','B') (i.e., 'A' and 'B' simultaneously) and 'A' are
incomparable in terms of consumption.


* IMPLEMENTATIVE TODOs:
- TODO: fix or make uniform the use of player's id, or (id, Player)
  pairs. This is confusing.
- TODO: pythonize and modernize code.

'''


from multiset import FrozenMultiset, Multiset
import itertools
from collections import Counter

FIRST_PLAYER_ID = 1
COMPLEX = True  # True to work with complex resources, e.g., ('A', 'B')


def _submultisets(multiset):
    counts = Counter([r for r in multiset])
    for sc in itertools.product(*[range(n + 1) for n in counts.values()]):
        yield FrozenMultiset(Counter(dict(zip(counts.keys(), sc))))


def _restriction(multiset1, multiset2):
    '''
    returns the submultiset of multiset1 restricted to the elements
    present in multiset2.
    '''
    flat1 = Multiset([r for r in [res for res in multiset1]])
    flat2 = Multiset([r for r in [res for res in multiset2]])
    return Multiset([r for r in flat1 if r in flat2])


class NotAProfileException(Exception):
    def __init__(self, message):
        self.message = message


class LoopingComputationsException(RuntimeError):
    def __init__(self, message):
        self.message = message


class Player:
    '''
    Player configuration, with name, endowment, and goal.
    Endowment and goal are multisets of hashable objects.
    Use as:
    Player("Ann", ['gold','ice','bucket','ice'], ['diamond'])
    Player("Ann", [6,4,4,3,4], [2])
    Player("Ann", ['A','H','F','N','B','B'], ['Z','B'])
    Last is equivalent to:
    Player("Ann", 'AHFNBB', 'ZB')
    '''

    def __init__(self, name, endowment, goal):
        self.name = name
        self.endowment = FrozenMultiset(endowment)
        self.goal = FrozenMultiset(goal)

    def describe(self):
        print("Name: %s" % self.name)
        print("Endowment: %s" % str(self.endowment))
        print("Goal: %s" % str(self.goal))

    def __str__(self):
        return "Player: Name: %s. Endowment: %s. Goal: %s." % (self.name,
                                                               self.endowment,
                                                               self.goal)

    def __repr__(self):
        return "Player: Name: %s. Endowment: %s. Goal: %s." % (self.name,
                                                               self.endowment,
                                                               self.goal)


class Game:

    class Profile():
        def __init__(self, game, choices):
            if not Game.Profile.is_profile(game, choices):
                raise NotAProfileException(
                    ("The object %s cannot be made "
                     "into a profile of %s.")
                    % (str(choices), str(game)))
            id = FIRST_PLAYER_ID
            self.profile = dict()
            for i in range(0, len(game.players)):
                self.profile[i + FIRST_PLAYER_ID] = choices[i]
                id = id + 1

        def is_profile(self, choices):
            if (len(choices) is not len(self.players)):
                return False
            for i in range(0, len(self.players)):
                id, p = self.players[i]
                if (not choices[i].issubset(p.endowment)):
                    return False
            return True

        def get(self, n):
            return self.profile[n]

        def __str__(self):
            return "Profile: %s" % [(id, str(self.profile[id]))
                                    for id in self.profile]

        def __hash__(self):
            return hash(str(self.profile))

        def __eq__(self, other):
            return (self.profile.keys() == other.profile.keys()
                    and all(self.profile[k] == other.profile[k]
                            for k in self.profile.keys()))

    def __init__(self, *players):
        id = FIRST_PLAYER_ID
        p = list()
        for player in players:
            if (not isinstance(player, Player)):
                raise TypeError(f"{player} must be a Player")
            p.append((id, player))
            id = id + 1
        self.players = list(p)

    def __str__(self):
        p = list()
        for player in self.players:
            p.append("%s" % str(player))
        return "Game: %s" % p

    def create_profile(self, choices):
        return Game.Profile(self, choices)

    def outcome(self, profile, complex=False):
        '''Supports lists (bundles) of atomic resources with complex=True.
        ann = Player("Ann", ['B', ('B','C'), 'B'], ['B','B'])
        bob = Player("Bob", 'B', ['B', 'K'])
        game = Game(ann, bob)
        cann = FrozenMultiset(['B', ('B','C')])
        cbob = FrozenMultiset('B')
        profile = game.create_profile((cann,cbob))
        print(game.outcome(profile))
        >>> {B, B, B, C}
        With complex=False, we get:
        >>> {B, B, ('B', 'C')}
        where ('B','C') is still seen as an atomic resource.

        Warning: A resource ('A', ('C','B')), will consider ('C','B')
        as an atomic resource.

        '''
        outcome = Multiset()
        for i in range(FIRST_PLAYER_ID, len(self.players) + FIRST_PLAYER_ID):
            if (complex):
                for r in profile.get(i):
                    if (isinstance(r, tuple)):
                        for a in r:
                            outcome.add(a)
                    else:
                        outcome.add(r)
            else:
                outcome.update(profile.get(i))
        return outcome

    def is_individually_satisfied(self, player, profile):
        outcome = self.outcome(profile, complex=COMPLEX)
        id, p = player
        return p.goal.issubset(outcome)

    def is_contentious(self, profile):
        outcome = self.outcome(profile, complex=COMPLEX)
        combined_goals = Multiset()
        for player in self.players:
            if self.is_individually_satisfied(player, profile):
                id, p = player
                combined_goals.update(p.goal)
        return not combined_goals.issubset(outcome)

    def is_privately_contentious(self, profile, player):
        '''
        *player* is a (id, Player) pair in the game.
        TODO: could be moved to the Player class.
        '''
        outcome = self.outcome(profile, complex=COMPLEX)
        if not self.is_individually_satisfied(player, profile):
            return False
        ref_id, ref_p = player
        combined_goals = Multiset()
        for ply in self.players:
            if self.is_individually_satisfied(ply, profile):
                id, p = ply
                combined_goals.update(_restriction(p.goal, ref_p.goal))
        return not combined_goals.issubset(outcome)

    def prefers_dct(self, player, profile1, profile2):
        '''
        Dichotomous and contention-tolerant preferences.
        *player* is a (id, Player) pair in the game.
        '''
        id, p = player
        like1 = self.is_individually_satisfied(player, profile1)
        like2 = self.is_individually_satisfied(player, profile2)
        return (like1 and not like2)

    def prefers_pct(self, player, profile1, profile2):
        '''
        Parsimonious and contention-tolerant preferences.
        *player* is a (id, Player) pair in the game.
        '''
        id, p = player
        choice1 = profile1.get(id)
        choice2 = profile2.get(id)
        like1 = self.is_individually_satisfied(player, profile1)
        like2 = self.is_individually_satisfied(player, profile2)
        subset1 = choice1.issubset(choice2)
        subset2 = choice2.issubset(choice1)
        subset = subset1 and not subset2
        return ((like1 and not like2)
                or (like1 and like2 and subset)
                or (not like1 and not like2 and subset))

    def prefers_dca(self, player, profile1, profile2):
        '''
        Dichotomous and contention-averse preferences.
        *player* is a (id, Player) pair in the game.
        '''
        id, p = player
        like1 = self.is_individually_satisfied(player, profile1)
        like2 = self.is_individually_satisfied(player, profile2)
        contentious1 = self.is_contentious(profile1)
        contentious2 = self.is_contentious(profile2)
        good1 = like1 and not contentious1
        good2 = like2 and not contentious2
        return (good1 and not good2)

    def prefers_pca(self, player, profile1, profile2):
        '''
        Parsimonious and contention-averse preferences.
        *player* is a (id, Player) pair in the game.
        '''
        id, p = player
        choice1 = profile1.get(id)
        choice2 = profile2.get(id)
        like1 = self.is_individually_satisfied(player, profile1)
        like2 = self.is_individually_satisfied(player, profile2)
        contentious1 = self.is_contentious(profile1)
        contentious2 = self.is_contentious(profile2)
        good1 = like1 and not contentious1
        good2 = like2 and not contentious2
        subset1 = choice1.issubset(choice2)
        subset2 = choice2.issubset(choice1)
        subset = subset1 and not subset2
        return ((good1 and not good2)
                or (good1 and good2 and subset)
                or (not good1 and not good2 and subset))

    def prefers_dpca(self, player, profile1, profile2):
        '''
        Dichotomous and private contention-averse preferences.
        *player* is a (id, Player) pair in the game.
        '''
        id, p = player
        like1 = self.is_individually_satisfied(player, profile1)
        like2 = self.is_individually_satisfied(player, profile2)
        contentious1 = self.is_privately_contentious(profile1, player)
        contentious2 = self.is_privately_contentious(profile2, player)
        good1 = like1 and not contentious1
        good2 = like2 and not contentious2
        return (good1 and not good2)

    def prefers_ppca(self, player, profile1, profile2):
        '''
        Parsimonious and private contention-averse preferences.
        *player* is a (id, Player) pair in the game.
        '''
        id, p = player
        choice1 = profile1.get(id)
        choice2 = profile2.get(id)
        like1 = self.is_individually_satisfied(player, profile1)
        like2 = self.is_individually_satisfied(player, profile2)
        contentious1 = self.is_privately_contentious(profile1, player)
        contentious2 = self.is_privately_contentious(profile2, player)
        good1 = like1 and not contentious1
        good2 = like2 and not contentious2
        subset1 = choice1.issubset(choice2)
        subset2 = choice2.issubset(choice1)
        subset = subset1 and not subset2
        return ((good1 and not good2)
                or (good1 and good2 and subset)
                or (not good1 and not good2 and subset))

    def choices(self, player):
        '''Returns the set of choices available to *player*.

        *player* is a (id, Player) pair in the game.
        '''
        id, p = player
        chs = set()
        for i in range(0, len(p.endowment) + 1):
            for ch in itertools.combinations(p.endowment, i):
                chs.add(FrozenMultiset(ch))
        # As of now, with the current implementation of submultisets,
        # using itertools.combinations, even though it does not work
        # greatly with Multiset, is faster than using
        # choices_generator to create the whole set.
        return chs

    def choices_generator(self, player):
        '''Returns the set of choices available to *player*.

        *player* is a (id, Player) pair in the game.
        '''
        id, p = player
        # using itertools.combinations like in the function choices is
        # not a good solution for a generator, because it generates
        # duplicates when used with multisets.
        return _submultisets(p.endowment)

    def profile_generator(self):
        '''
        Generates the set of profiles in the game.
        '''
        choices = [self.choices_generator(player) for player in self.players]
        for chs in itertools.product(*choices):
            profile = Game.Profile(self, chs)
            yield profile

    def deviate(self, profile, id, choice):
        '''Returns the Profile resulting from the deviation from *profile* of
        player with identifier *id* to the choice *choice*.

        '''
        new_choices = ()
        for i in range(FIRST_PLAYER_ID, id):
            new_choices = new_choices + (profile.get(i),)
        new_choices = new_choices + (choice,)
        for i in range(id + 1, len(self.players) + FIRST_PLAYER_ID):
            new_choices = new_choices + (profile.get(i),)
        return Game.Profile(self, new_choices)

    def exists_profitable_deviation_generic(self, player, profile,
                                            fun_prefers, verbose=False):
        '''Decides whether there is a profitable deviation from *profile* for
        *player* with *fun_prefers* type of preferences.

        *player* is a (id, Player) pair in the game.

        '''
        id, p = player
        for ch in self.choices_generator(player):
            new_profile = self.deviate(profile, id, ch)
            if (fun_prefers(player, new_profile, profile)):
                if (verbose):
                    print("From", profile, "\n", p.name,
                          "prefers", new_profile)
                return (True, ch)
        return (False, None)

    def is_nash_generic(self, profile, fun_prefers, verbose=False):
        '''Decide whether a profile is a Nash equilibrium with *fun_prefers*
        type of preferences.

        '''
        return not any(
            self.exists_profitable_deviation_generic(player,
                                                     profile,
                                                     fun_prefers,
                                                     verbose)[0]
            for player in self.players)

    def is_nash_adct(self, profile, verbose=False):
        '''Decide whether a profile is a Nash equilibrium in *affine*
        individual resource games, with dichotomous and
        contention-tolerant preferences.

        See Algorithm 2 in arXiv:1802.00257v1

        '''
        for player in self.players:
            id, p = player
            like = self.is_individually_satisfied(player, profile)
            if (like):
                continue
            else:
                newchoice = p.endowment
                newprofile = self.deviate(profile, id, newchoice)
                newlike = self.is_individually_satisfied(player, newprofile)
                if (newlike):
                    return False
        return True

    def is_nash_apct(self, profile, verbose=False):
        '''Decide whether a profile is a Nash equilibrium in *affine*
        individual resource games, with parsimonious and
        contention-tolerant preferences.

        See Algorithm 6 in arXiv:1802.00257v1

        '''
        for player in self.players:
            id, p = player
            like = self.is_individually_satisfied(player, profile)
            if (like):
                for a in profile.get(id):
                    newchoice = Multiset(profile.get(id))
                    occ = newchoice.remove(a)
                    if (occ != 1):
                        newchoice.update(Multiset([a for i in range(1, occ)]))
                    newprofile = self.deviate(profile, id, newchoice)
                    newlike = self.is_individually_satisfied(player,
                                                             newprofile)
                    if (newlike):
                        return False
            else:
                newchoice = p.endowment
                newprofile = self.deviate(profile, id, newchoice)
                newlike = self.is_individually_satisfied(player, newprofile)
                if (newlike):
                    return False
                if (not len(profile.get(id)) == 0):
                    return False
        return True

    def nash_generator_adct(self):
        for profile in self.profile_generator():
            if self.is_nash_adct(profile):
                yield profile

    def nash_generator_apct(self):
        for profile in self.profile_generator():
            if self.is_nash_apct(profile):
                yield profile

    def nash_generator_generic(self, fun_prefers, verbose=False):
        for profile in self.profile_generator():
            if self.is_nash_generic(profile, fun_prefers, verbose):
                yield profile

    def __deficit_ppca(self, profile, player):
        contenders = [(ci, cp) for (ci, cp) in self.players
                      if self.is_privately_contentious(profile, (ci, cp))]
        cont_goals = Multiset()
        for (ci, cp) in contenders:
            cont_goals.update(_restriction(cp.goal, player.goal))
        deficit = cont_goals.difference(self.outcome(profile))
        return deficit

    def __exists_profitable_deviation_from_empty_ppca_irgbar(self, player, profile):
        '''Decides whether there is a profitable deviation from *profile* for
        *player*, in the case of IRGBAR (IRGs with endowments as bags of
        atomic resources) and affine, private contention averse
        preferences. Returns a profitable deviation if it exsits.

        The choice of *player* in *profile* must be empty.

        *player* is a (id, Player) pair in the game.

        '''
        id, p = player
        if (len(profile.get(id)) != 0):
            raise ValueError(f"Choice of player {p} with id {id} must be empty in {profile}.")
        miss = p.goal.difference(self.outcome(profile))
        if (not miss.issubset(p.endowment)):
            return (False, None)
        pchoice = Multiset(miss)
        pendow = p.endowment.copy().difference(pchoice)
        newprofile = self.deviate(profile, id, pchoice)
        deficit = self.__deficit_ppca(newprofile, p)
        while (deficit.issubset(pendow) and len(deficit) != 0):
            pchoice.update(deficit)
            pendow = pendow.difference(deficit)
            newprofile = self.deviate(newprofile, id, pchoice)
            deficit = self.__deficit_ppca(newprofile, p)
        return ((len(newprofile.get(id)) != 0 and
                 self.is_individually_satisfied(player, newprofile) and
                 not self.is_privately_contentious(newprofile, player)),
                pchoice)

    def exists_profitable_deviation_ppca_irgbar(self, player, profile):
        '''Decides whether there is a profitable deviation from *profile* for
        *player*, in the case of IRGBAR (IRGs with endowments as bags of
        atomic resources) and affine, private contention averse
        preferences. Returns a profitable deviation if it exsits.

        *player* is a (id, Player) pair in the game.
        '''
        id, p = player
        if (not self.is_individually_satisfied(player, profile) or
           self.is_privately_contentious(profile, player)):  # pca ppca differ here
            if (len(profile.get(id)) != 0):
                return (True, Multiset())
            else:
                newprofile = self.deviate(profile, id, Multiset())
                return self.__exists_profitable_deviation_from_empty_ppca_irgbar(player,
                                                                                 newprofile)
        else:
            # maybe happy in empty already
            if (len(profile.get(id)) == 0):
                return (False, None)
            # maybe empty is good deviation
            newprofile = self.deviate(profile, id, Multiset())
            if (not self.is_privately_contentious(newprofile, player) and  # pca ppca differ here
                self.is_individually_satisfied(player, newprofile)):
                return (True, Multiset())
            # otherwise, exists dev from empty in trimmed game?
            player_with_new_endowment = Player(f"{p.name} [trimmed endowment]",
                                               profile.get(id),
                                               p.goal)
            players = [p for (i, p) in self.players].copy()
            players[id - FIRST_PLAYER_ID] = player_with_new_endowment
            newgame = Game(*players)
            newprofile = newgame.deviate(profile, id, Multiset())
            (b, c) = newgame.__exists_profitable_deviation_from_empty_ppca_irgbar(
                (id, player_with_new_endowment),
                newprofile)
            return (b and c != profile.get(id), c)

    def is_nash_ppca_irgbar(self, profile):
        '''Decide whether a profile is a Nash equilibrium,
        in the case of IRGBAR (IRGs with endowments as bags of
        atomic resources) and affine, private contention averse
        preferences.
        '''
        return not any(
            self.exists_profitable_deviation_ppca_irgbar(player, profile)[0]
            for player in self.players)

    def nash_generator_ppca_irgbar(self):
        for profile in self.profile_generator():
            if self.is_nash_ppca_irgbar(profile):
                yield profile

    def find_nash_ppca_irgbar(self):
        '''Tries to find a Nash equilibrium in an IRGBAR with parsimonious
        private contention preferences. Iterated best response
        algorithm from the empty profile. It returns a Nash
        equilibrium profile when it finds one. Does not always
        terminate with a solution, and raises
        LoopingComputationsException if it enters a loop.

        '''
        trajectory = []
        profile = self.create_profile([Multiset() for _ in self.players])
        while (not self.is_nash_ppca_irgbar(profile)):
            for (id, p) in self.players:
                if((profile, id) in trajectory):
                    trajectory.append((profile, id))
                    raise LoopingComputationsException(f"Entering a loop {[(str(pp), str(ii)) for (pp, ii) in trajectory]}.")
                trajectory.append((profile, id))
                (b, c) = self.exists_profitable_deviation_ppca_irgbar((id, p),
                                                                      profile)
                if (b):
                    profile = self.deviate(profile, id, c)
        return profile

    def __deficit_pca(self, profile, player):
        # although we are in the case of public contention,
        # is_privately_contentious is intended to identify the
        # contenders.
        contenders = [(ci, cp) for (ci, cp) in self.players
                      if self.is_privately_contentious(profile, (ci, cp))]
        cont_goals = Multiset()
        for (ci, cp) in contenders:
            cont_goals.update(cp.goal)
        deficit = cont_goals.difference(self.outcome(profile))
        return deficit

    def __exists_profitable_deviation_from_empty_pca_irgbar(self, player, profile):
        '''Decides whether there is a profitable deviation from *profile* for
        *player*, in the case of IRGBAR (IRGs with endowments as bags of
        atomic resources) and affine, public contention averse
        preferences. Returns a profitable deviation if it exsits.

        The choice of *player* in *profile* must be empty.

        *player* is a (id, Player) pair in the game.

        '''
        id, p = player
        if (len(profile.get(id)) != 0):
            raise ValueError(f"Choice of player {p} with id {id} must be empty in {profile}.")
        miss = p.goal.difference(self.outcome(profile))
        if (not miss.issubset(p.endowment)):
            return (False, None)
        pchoice = Multiset(miss)
        pendow = p.endowment.copy().difference(pchoice)
        newprofile = self.deviate(profile, id, pchoice)
        deficit = self.__deficit_pca(newprofile, p)
        while (deficit.issubset(pendow) and len(deficit) != 0):
            pchoice.update(deficit)
            pendow = pendow.difference(deficit)
            newprofile = self.deviate(newprofile, id, pchoice)
            deficit = self.__deficit_pca(newprofile, p)
        return ((len(newprofile.get(id)) != 0 and
                 self.is_individually_satisfied(player, newprofile) and
                 not self.is_contentious(newprofile)),
                pchoice)

    def exists_profitable_deviation_pca_irgbar(self, player, profile):
        '''Decides whether there is a profitable deviation from *profile* for
        *player*, in the case of IRGBAR (IRGs with endowments as bags of
        atomic resources) and affine, public contention averse
        preferences. Returns a profitable deviation if it exsits.

        *player* is a (id, Player) pair in the game.
        '''
        id, p = player
        if(not self.is_individually_satisfied(player, profile) or
           self.is_contentious(profile)):  # pca ppca differ here
            if (len(profile.get(id)) != 0):
                return (True, Multiset())
            else:
                newprofile = self.deviate(profile, id, Multiset())
                return self.__exists_profitable_deviation_from_empty_pca_irgbar(player,
                                                                                newprofile)
        else:
            # maybe happy in empty already
            if (len(profile.get(id)) == 0):
                return (False, None)
            # maybe empty is good deviation
            newprofile = self.deviate(profile, id, Multiset())
            if (not self.is_contentious(newprofile) and  # pca ppca differ here
                self.is_individually_satisfied(player, newprofile)):
                return (True, Multiset())
            # otherwise, exists dev from empty in trimmed game?
            player_with_new_endowment = Player(f"{p.name} [trimmed endowment]",
                                               profile.get(id), p.goal)
            players = [p for (i, p) in self.players].copy()
            players[id - FIRST_PLAYER_ID] = player_with_new_endowment
            newgame = Game(*players)
            newprofile = newgame.deviate(profile, id, Multiset())
            (b, c) = newgame.__exists_profitable_deviation_from_empty_pca_irgbar(
                (id, player_with_new_endowment),
                newprofile)
            return (b and c != profile.get(id), c)

    def is_nash_pca_irgbar(self, profile):
        '''Decide whether a profile is a Nash equilibrium, in the case of
        IRGBAR (IRGs with endowments as bags of atomic resources) and
        affine, public contention averse preferences.

        '''
        return not any(
            self.exists_profitable_deviation_pca_irgbar(player, profile)[0]
            for player in self.players)

    def nash_generator_pca_irgbar(self):
        for profile in self.profile_generator():
            if self.is_nash_pca_irgbar(profile):
                yield profile

    def find_nash_pca_irgbar(self):
        '''Iterated best response algorithm from the empty profile. Returns a
        Nash equilibrium in an IRGBAR with parsimonious public
        contention preferences.

        '''
        trajectory = []
        profile = self.create_profile([Multiset() for _ in self.players])
        while (not self.is_nash_pca_irgbar(profile)):
            for (id, p) in self.players:
                if((profile, id) in trajectory):
                    raise LoopingComputationsException(f"Unexpected loop: {[(str(pp), str(ii)) for (pp, ii) in trajectory]}.")
                trajectory.append((profile, id))
                (b, c) = self.exists_profitable_deviation_pca_irgbar((id, p),
                                                                     profile)
                if (b):
                    profile = self.deviate(profile, id, c)
        return profile
